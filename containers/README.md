# O que são Containers?

Um pacote de código que pode executar uma ação por exemplo: rodar uma aplicação de **Node.js**, **PHP**, **Python** e etc.

Ou seja, os nossos projetos serão executados dentro dos containers que criamos/utilizamos.

## Comandos:

_Containers_ utilizam **imagens** para poderem ser executados.

- Executar uma imagem em um container:

`docker run <imagem>`

> Exemplo: `docker run php`

- Verificar containers executados ou rodando:
- Lista todos containers rodando `docker ps`
- Lista todos containers executados, rodando ou parados `docker ps -a`

- Executar containers com interação:

Algumas imagens permitem usar interação como **ubuntu**, **python**, **node**, ...

- `docker run -it <imagem>`

> Exemplo: ``docker run -it ubuntu `

- Executar containers em background:

Podemos executar containers em background para não ficar ocupando um terminal durante sua execução.

A **flag -d** (detached) permite rodar em background.

`docker run -d <imagem>`

- Expondo portas:

Podemos expor portas para ter conexão com um container.

A **flag -p** permite expor portas.

`docker run -d -p 80:80 nginx`

> **OBS:** a primeira porta é a do host e a segunda porta é a do container então (host/pc):(container).

- Parando containers :

Podemos para um container com o comando:

`docker stop <id ou nome>`

> Para verificar se o container parou: `docker ps`

- Iniciando containers:

Aprendemos já a parar um container com o stop, para voltar a rodar:

`docker run <id ou nome>`

> **OBS:** Lembre-se que o **run** sempre cria um novo container.  
> então caso seja necessário aproveitar um container antigo opte pelo **start**.

- Definindo nome ao container:

Podemos definir um nome do container com a **flag --name**.

`docker run --name nome-do-container python`

- Verificando os logs:

Podemos verificar o que aconteceu em um container com o cmando logs.

`docker logs <id ou nome>`

> As últimas ações realizadas no container, serão exibidas no terminal.

- Removendo containers:

Podemos remover um container da máquina que estamos executando o docker.

`docker -rm <id ou nome>`

> **OBS:** Se o container estiver rodando ainda, podemos utilizar a **flag -f** (force).  
> O container removido não é mais listado em **`docker ps -a`**.
